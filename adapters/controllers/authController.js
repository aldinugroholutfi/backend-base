import login from '../../application/use_cases/auth/login';
import logout from '../../application/use_cases/auth/logout';

export default function authController(
  userDbRepository,
  userDbRepositoryImpl,
  authServiceInterface,
  authServiceImpl
) {
  const dbRepository = userDbRepository(userDbRepositoryImpl());
  const authService = authServiceInterface(authServiceImpl());

  const loginUser = (req, res, next) => {
    const { email, password } = req.body;
    login(email, password, dbRepository, authService)
    .then((result) => {
      const { token, user } = result;
      res.json({ token, user });
    })
      .catch((err) => next(err));
  };
  const logoutUser = (req, res, next) => {
    const { token } = req.body;
    logout(token, dbRepository, authService)
        .then((result) => {
          res.json({ result});
        })
        .catch((err) => next(err));
  }
  return {
    loginUser,
    logoutUser
  };
}
