export default function authService(service) {
  const expiredTokens = new Set();
  const encryptPassword = (password) => service.encryptPassword(password);

  const compare = (password, hashedPassword) =>
    service.compare(password, hashedPassword);

  const verify = (token) => service.verify(token);

  const generateToken = (payload) => service.generateToken(payload);

  const expireToken = (token) => {
    expiredTokens.add(token); // Add the token to the Set to mark it as expired
  };

  const isTokenExpired = (token) => {
    return expiredTokens.has(token); // Check if the token is in the Set
  };

  return {
    encryptPassword,
    compare,
    verify,
    generateToken,
    expireToken,
    isTokenExpired
  };
}
