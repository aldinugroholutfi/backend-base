export default function logout(token, userRepository, authService) {
    return new Promise((resolve, reject) => {
        authService.expireToken(token);
        resolve('Token revoked successfully');
    });
}